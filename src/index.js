import _ from 'lodash';
import Vue from 'vue';
import Mapbox from 'mapbox-gl-vue';
import MapboxLanguage from '@mapbox/mapbox-gl-language';
import 'mapbox-gl/dist/mapbox-gl.css';
import './index.scss';
// import GreenIcon from "./img/Green.svg";
// import OrangeIcon from "./img/Orange.svg";
// import RedIcon from "./img/Red.svg";
// import GreenBadgeIcon from "./img/GreenBadge.svg";
// import OrangeBadgeIcon from "./img/OrangeBadge.svg";
// import RedBadgeIcon from "./img/RedBadge.svg";
// import DarkIcon from "./img/Dark.svg";
// import vIcon from "./img/v.svg";
import GetAccidentIcon from "./getAccidentIcon";

function leadZero(str, length) {
  while (str.length < length) {
    str = "0" + str;
  }
  return str;
}

function humanizeDate(date) {
  return `${leadZero(date.getDate().toString(), 2)}.${leadZero((date.getMonth() + 1).toString(), 2)}.${date.getFullYear().toString()}`;
}

function strftime(date, str) {
  let ret = str;
  let mask = {};
  mask["%y"] = leadZero(date.getFullYear().toString(), 2);
  mask["%m"] = leadZero((date.getMonth() + 1).toString(), 2);
  mask["%d"] = leadZero(date.getDate().toString(), 2);
  mask["%H"] = leadZero(date.getHours().toString(), 2);
  mask["%M"] = leadZero(date.getMinutes().toString(), 2);
  mask["%S"] = leadZero(date.getSeconds().toString(), 2);
  mask["%s"] = leadZero(date.getMilliseconds().toString(), 3);
  let keys = Object.keys(mask);
  for (let i in keys) {
    while (true) {
      if (ret.match(keys[i])) {
        ret = ret.replace(keys[i], mask[keys[i]]);
      }
      else {
        break;
      }
    }
  }
  return ret;
}

function parseDate(str, pattern) {
  if (str.length != pattern.length) {
    console.error(`[parseDate] input string and pattern must have equal length`);
    return null;
  }
  else {
    let obj = {
      y: "",
      m: "",
      d: "",
      H: "",
      M: "",
      S: ""
    };
    let patternSymbols = "ymdHMS";
    for (let i in str) {
      if (patternSymbols.includes(pattern[i])) {
        obj[pattern[i]] += str[i];
        continue;
      }
      else if (str[i] == pattern[i]) {
        continue;
      }
      else {
        console.error(`[parseDate] input string at ${i} symbol (${str[i]}) doesn't match with pattern symbol (${pattern[i]})`);
        return null;
      }
    }
    for (let i in obj) {
      if (obj[i].length == 0) {
        obj[i] = "0";
      }
    }
    return new Date(parseInt(obj.y), parseInt(obj.m) - 1, parseInt(obj.d), parseInt(obj.H), parseInt(obj.M), parseInt(obj.S));
  }
}

function ready() {

  let appDiv = document.createElement("div");
  appDiv.id = "app";
  document.body.appendChild(appDiv);
  appDiv.innerHTML = `
  <mapbox
    access-token="pk.eyJ1IjoiZWlrNCIsImEiOiJjamZpZmhrYzc0YmN3MndtbTBpMHl3YmpuIn0.lj74B5GWbzPWopt0lRp8Ww"
    :map-options="{
      style: 'mapbox://styles/mapbox/streets-v9',
      center: [37.37561676343762, 55.58333873968863],
      zoom: 8.57
    }"
    @map-moveend="mapMoveend"
    @map-load="mapLoad"
    @map-click="mapClick"
    @map-mousemove="mapMousemove"
    @map-init="mapInit"
  ></mapbox>
  <div class="tooltip" v-if="tooltip.show" v-bind:style="[{'left': tooltip.x+'px'}, {'top': tooltip.y+'px'}]">
    <table>
      <tbody>
        <tr>
          <th>Место:</th>
          <td>{{tooltip.name}}</td>
        </tr>
        <tr>
          <th>Количество ДТП:</th>
          <td>{{tooltip.count}}</td>
        </tr>
        <tr>
          <th>Вероятность:</th>
          <td>{{(tooltip.percent*100).toFixed(2)}}%</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="legend">
    <div class="title">
      <div class="crashIcon"></div>
      <div class="text">Вероятность ДТП</div>
      <div class="arrowDown" @click="showTable = !showTable"></div>
    </div>

    <table class="table" v-if="showTable">
      <tbody>
        <tr>
          <td><div class="greenCircle"></div></td>
          <td class="left">Низкая</td>
        </tr>
        <tr>
          <td><div class="orangeCircle"></div></td>
          <td class="left">Средняя</td>
        </tr>
        <tr>
          <td><div class="redCircle"></div></td>
          <td class="left">Высокая</td>
        </tr>
        <tr>
          <td class="flex"><div class="badgeCircle"><span>3</span></div></td>
          <td class="left">Количество ДТП произошедших за текущие сутки</td>
        </tr>
      </tbody>
    </table>

    <div class="timeTableFrame range" v-if="showRange">
      <div class="timeTableFrame__item"><input class="timeTableFrame-item__range" type="range" v-model="value" v-bind:min="0" v-bind:max="maxValue"></div>
      <div class="timeTableFrame__item"><input v-bind:class="['timeTableFrame-item__text', {'timeTableFrame-item__text_red': timeStatus == 'wrong format'}, {'timeTableFrame-item__text_orange': timeStatus == 'out of bounds'}, {'timeTableFrame-item__text_yellow': timeStatus == 'wrong date'}]" type="text" v-model="timeText" @input="timeInput"></div>
      <div class="timeTableFrame__item"><div v-bind:class="['button', {'button_disabled': timeStatus != 'good'}]" @click="newTime">Показать ДТП за выбранное время</div></div>
    </div>


    <div class="timeTableFrame" v-if="arr.length > 0">
      <table class="timeTable">
        <tbody id="timeTable">
          <tr v-for="i in arr">
            <td class="icon"></td>
            <td class="time">{{i}}</td>
          </tr>
        </tbody>
      <table>
    </div>
  </div>`;

  let app = new Vue({
    el: '#app',
    data: {
      answer: null,
      answer1: null,
      map: null,
      markers: [],
      tickrate: 1000 * 15,
      url: "https://niikeeper.com/accidentsAPI/v0.1.0/historicalForecast?ts=",
      url1: "https://niikeeper.com/nii_api/v0.7.8/codd/adm_data_taxi",
      timeLimits: "https://niikeeper.com/accidentsAPI/v0.1.0/timeLimits",
      test: "1.2.3",
      showTable: true,
      arr: [],
      minDate: null,
      maxDate: null,
      minHours: null,
      maxHours: null,
      maxValue: 0,
      value: 0,
      timeText: "",
      valueDate: null,
      timeStatus: "good",
      showRange: false,
      urlPart: "",
      tooltip: {
        count: null,
        name: null,
        percent: null,
        x: 0,
        y: 0,
        show: false
      },
    },
    components: {
      Mapbox
    },
    watch: {
      value: function (n,o) {
        let d = parseDate(strftime(this.minDate, "%y %m %d %H"), "yyyy mm dd HH");
        let h = d.getHours();
        d.setHours(parseInt(h) + parseInt(n));
        this.timeText = strftime(d, "%d.%m.%y %H:00");
        this.timeStatus = "good";
        this.valueDate = d;
      },
    },
    methods: {

      timeInput() {
        let s = this.timeText.search(/^\d\d\.\d\d\.\d\d\d\d \d\d:\d\d$/);
        if (s == -1) {
          this.timeStatus = "wrong format";
        }
        else {
          let d = parseDate(this.timeText, "dd.mm.yyyy HH:MM");
          if (strftime(d, "%d.%m.%y %H:00") != this.timeText) {
            this.timeStatus = "wrong date";
          }
          else {
            let hours = d.getTime()/(1000 * 60 * 60);
            if (hours >= this.minHours && hours <= this.maxHours) {
              this.timeStatus = "good";
              this.value = hours - this.minHours;
            }
            else {
              this.timeStatus = "out of bounds";
            }
          }
        }
      },

      newTime() {
        if (this.timeStatus == "good") {
          if (this.urlPart != strftime(this.valueDate, "%y-%m-%dT%H:00:00")) {
            this.urlPart = strftime(this.valueDate, "%y-%m-%dT%H:00:00");
            this.updateMap();
          }
        }
      },

      draw() {
        if (this.markers.length > 0) {
          for (let i in this.markers) {
            this.markers[i].remove();
          }
          this.markers = [];
          this.markersIDs = [];
        }
        let geoJSON = JSON.parse(this.answer);
        for (let i in geoJSON.features) {
          let className = "marker";
          let icon = GetAccidentIcon(geoJSON.features[i].properties, this);
          let div = document.createElement("div");
          div.innerHTML = icon;
          div.className = className;
          let self = this;
          let marker = new mapboxgl.Marker({
            element: div
          });
          marker.setLngLat(geoJSON.features[i].geometry.coordinates);
          marker.addTo(this.map);
          this.markers.push(marker);
          let p = geoJSON.features[i].properties;
          // if (p.percent < 0) {
          //   console.log(geoJSON.features[i]);
          // }
          div.onmouseover = function () {
            self.tooltip.show = true;
            self.tooltip.name = p.name;
            self.tooltip.percent = p.percent;
            self.tooltip.count = p.count;
          }
          div.onmousemove = function (e) {
            self.tooltip.x = e.clientX + 10;
            self.tooltip.y = e.clientY + 10;
          }
          div.onmouseout = function () {
            self.tooltip.show = false;
            self.tooltip.x = 0;
            self.tooltip.y = 0;
          }
          div.onclick = function () {
            while (self.arr.length > 0) {
              self.arr.pop();
            }
            if (geoJSON.features[i].properties.data[0]) {
              for (let j in geoJSON.features[i].properties.data) {
                self.arr.push(geoJSON.features[i].properties.data[j]);
              }
            }
            else {
              while (self.arr.length > 0) {
                self.arr.pop();
              }
            }
          }
        }
      },

      updateMap() {
        //console.log("update");
        (async () => {
          try {

            let response = await fetch(this.url+this.urlPart, {
              method: "GET"
            });
            let answer = await response.json();

            this.answer = JSON.stringify(answer);

            this.draw();

            let response1 = await fetch(this.url1, {
              method: "GET",
              mode: "cors"
            });
            // this.answer1 = await response1.json();
            let a = await response1.json();
            if (!this.answer1) {
              this.answer1 = a;
            }
            else {
              for (let i in a.moscow) {
                if (a.moscow[i] != 0) {
                  let aa = a.moscow[i].toString();
                  this.answer1.moscow[i] = aa.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                }
              }
            }
          }
          catch (e) {
            console.log("fetch error", e);
          }
        })();
      },

      mapMoveend(map) {
        //console.log("current pos", map.getCenter(), map.getZoom());
      },

      mapLoad(map) {
        //console.log("load complete");
        (async () => {
          try {

            let response0 = await fetch(this.timeLimits, {
              method: "GET"
            });
            let answer0 = await response0.json();
            // console.log(answer0);
            this.minDate = parseDate(answer0.min, "yyyy-mm-dd HH:MM:SS");
            this.valueDate = parseDate(answer0.min, "yyyy-mm-dd HH:MM:SS");
            this.maxDate = parseDate(answer0.max, "yyyy-mm-dd HH:MM:SS");
            this.minHours = this.minDate.getTime()/(1000 * 60 * 60);
            this.maxHours = this.maxDate.getTime()/(1000 * 60 * 60);
            this.maxValue = this.maxHours - this.minHours;
            this.timeText = strftime(this.minDate, "%d.%m.%y %H:%M");
            this.showRange = true;
            this.urlPart = strftime(this.valueDate, "%y-%m-%dT%H:00:00");

            let response = await fetch(this.url+this.urlPart, {
              method: "GET"
            });
            let answer = await response.json();

            this.answer = JSON.stringify(answer);

            let response1 = await fetch(this.url1, {
              method: "GET",
              mode: "cors"
            });
            this.answer1 = await response1.json();

            for (let i in this.answer1.moscow) {
              let aa = this.answer1.moscow[i].toString();
              this.answer1.moscow[i] = aa.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
            }

            // this.init();

            this.draw();
            setInterval(this.updateMap, this.tickrate);
          }
          catch (e) {
            console.log("fetch error", e);
          }
        })();
      },

      mapClick(map, e) {

      },

      mapMousemove(map, e) { },

      mapInit(map) {
        this.map = map;
        let language = new MapboxLanguage({ defaultLanguage: 'ru' });
        map.addControl(language);
      }

    },

    computed: {}
  });
}

document.addEventListener("DOMContentLoaded", ready);